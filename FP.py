#!/usr/bin/python3
import pygame
from argparse import ArgumentParser
from typing import NamedTuple
from sys import exit
from random import randint, choice

class Input(NamedTuple):
  gameQuit: bool
  space: bool

class SwitchTimer(NamedTuple):
  switch: bool
  timer: int

class Images(NamedTuple):
  bg: pygame.Surface
  bgNight: pygame.Surface
  birdUp: pygame.Surface  
  floor: pygame.Surface
  birdAnimation: list
  pipe: pygame.Surface
  pipeRed: pygame.Surface

class Sounds(NamedTuple):
  hit: pygame.mixer.Sound
  flap: pygame.mixer.Sound
  score: pygame.mixer.Sound
  score2: pygame.mixer.Sound

class Pipes(NamedTuple):
  x: float
  height: float

class PipesRect(NamedTuple):
  top: pygame.Rect
  bottom: pygame.Rect

class Game(NamedTuple):
  smallResolution: bool
  mute: bool
  screen: pygame.Surface
  text: pygame.font.Font
  gameClock: pygame.time.Clock
  images: Images
  sounds: Sounds
  soundQueue: list  
  running: bool
  gameOver: bool
  freeze: int  
  bird_y: float
  bird_index: int
  birdupAnimation: SwitchTimer
  animationSpeed: int
  floor_x: float
  bg_x: float
  gravity: float
  score: int
  highScore: int
  doubleRound: bool
  pipes: Pipes

def eventsToInput(events: pygame.event):
  key = lambda event, key: event.type == pygame.KEYDOWN and event.key == key

  gameQuit = False
  space = False
  for event in events:
    gameQuit = True if gameQuit else True if event.type == pygame.QUIT else False
    gameQuit = True if gameQuit else True if key(event, pygame.K_ESCAPE) else False
    space = True if space else True if key(event, pygame.K_SPACE) else False
    space = True if space else True if event.type == pygame.MOUSEBUTTONDOWN else False
  return Input(
    gameQuit = gameQuit,
    space = space
  )

def init(mute: bool = False, smallResolution: bool = False):
  pygame.init()
  pygame.display.set_caption('Flappy BIRD UP!')
  scale2x = pygame.transform.scale2x
  load = pygame.image.load
  screen = pygame.display.set_mode((358,716) if smallResolution else (576,1024))
  midBird = scale2x(load('assets/bluebird-midflap.png').convert_alpha())

  if not mute:
    bg = pygame.mixer.Sound('sound/BG.ogg')
    bg.set_volume(0.25)
    bg.play(loops=-1)

  return Game(
    smallResolution = smallResolution,
    mute = mute,
    running = True,
    gameOver = False,
    screen = screen,
    text = pygame.font.Font('04B_19.ttf',40),
    gameClock = pygame.time.Clock(),
    bird_y = 400,
    bird_index = 0,
    animationSpeed = 0,
    birdupAnimation = SwitchTimer(switch=choice([True,False]), timer=250),
    floor_x = 0,
    bg_x = 0,
    gravity = 0.25,
    score = 0,
    highScore = 0,
    freeze = 0,
    doubleRound = False,
    pipes = Pipes(
      height = choice([400,600,800]),
      x = 800
    ),
    soundQueue = [],
    sounds = Sounds(
      hit = pygame.mixer.Sound('sound/sfx_hit.wav'),
      score = pygame.mixer.Sound('sound/sfx_point.wav'),
      score2 = pygame.mixer.Sound('sound/sfx_swooshing.wav'),
      flap = pygame.mixer.Sound('sound/sfx_wing.wav'),
    ),
    images = Images(
      bg = scale2x(load('assets/background-day.png').convert()),
      bgNight = scale2x(load('assets/background-night.png').convert()),
      birdUp = pygame.image.load('assets/BirdUp.png').convert_alpha(),      
      floor = scale2x(load('assets/base.png').convert()),
      birdAnimation = [
        scale2x(load('assets/bluebird-upflap.png').convert_alpha()),
        midBird,
        scale2x(load('assets/bluebird-downflap.png').convert_alpha()),
        midBird
      ],
      pipe = scale2x(load('assets/pipe-green.png').convert()),
      pipeRed = scale2x(load('assets/pipe-red.png').convert())
    )
  )

def pipesRect(game: Game):
  x = game.pipes.x
  height = game.pipes.height
  rect = game.images.pipe.get_rect
  return PipesRect(
    top = rect(midbottom = (x, height-300)),
    bottom = rect(midtop = (x, height))
  )

def update(game: Game, input: Input):
  def hasCollsion():
    def pipeCollision():
      birdRect = game.images.birdAnimation[0].get_rect(center = (100, game.bird_y))
      pipes = pipesRect(game)
      return birdRect.colliderect(pipes.top) or birdRect.colliderect(pipes.bottom)
    def borderCollision():
      y = game.bird_y+game.gravity
      return y >= 850 or y <= 0            
    return borderCollision() or pipeCollision()  
  def gameOver():
    return game._replace(
      gravity = 0,
      bird_y = 400,
      floor_x = 0 if game.floor_x <= -576 else game.floor_x-1,
      bg_x = 0 if game.bg_x <= -576 else game.bg_x-0.5,
      score = 0,
      pipes = Pipes(
        x = 800,
        height = choice([400,600,800])
      ),
      birdupAnimation = game.birdupAnimation._replace(
        timer = game.birdupAnimation.timer-1 if game.birdupAnimation.timer>0 else choice([150,500]),
        switch = choice([True, False]) if game.birdupAnimation.timer==0 else game.birdupAnimation.switch
      ),
      soundQueue = [],
      doubleRound = False,
      gameOver = False if input.space else game.gameOver,
    )
  def freeze():
    return game._replace(
      freeze = game.freeze-1,
      gameOver = True if game.freeze-1 == 0 else False,
      soundQueue = []
    )
  def quit():
    return game._replace(running = False)
  def next():
    collided = hasCollsion()
    pipePassed = game.pipes.x == 100
    nextPipe = game.pipes.x < -120
    nextAnimation = game.animationSpeed > 8
    birdAnimation = game.images.birdAnimation
    index = game.bird_index
    sound = game.sounds
    return game._replace(
      gravity = -9 if input.space else game.gravity+0.25,
      bird_y = game.bird_y+game.gravity,
      animationSpeed = 0 if nextAnimation else game.animationSpeed+1,
      bird_index = index if not nextAnimation else 0 if index+1 > len(birdAnimation)-1 else index+1,
      floor_x = 0 if game.floor_x <= -576 else game.floor_x-1,
      bg_x = 0 if game.bg_x <= -576 else game.bg_x-0.5,
      score = game.score+(2 if game.doubleRound else 1) if pipePassed else game.score,
      highScore = game.score if game.score > game.highScore else game.highScore,      
      freeze = 100 if collided else game.freeze,
      soundQueue = ([sound.hit] if collided else []) + 
        ([sound.flap] if input.space else []) + 
        ([sound.score2] if (pipePassed and game.doubleRound) else [sound.score] if pipePassed else []),
      doubleRound = randint(1, 50) > 45 if nextPipe else game.doubleRound,
      pipes = game.pipes._replace(
        x = 800 if nextPipe else game.pipes.x-5,
        height = choice([400,600,800]) if nextPipe else game.pipes.height
      )
    )

  return (
      quit() if input.gameQuit else
      freeze() if game.freeze > 0 else
      gameOver() if game.gameOver else
      next() 
    )

def draw(game: Game, replay: bool = False):
  orange = (252,111,3),
  white = (255,255,255)
  internalResolution = pygame.Surface((576, 1024))
  scale = pygame.transform.scale
  blit = internalResolution.blit
  flip = pygame.transform.flip
  text = game.text.render
  images = game.images

  if not game.mute:
    for sound in game.soundQueue:
      sound.play()

  if game.gameOver:
    blit(images.bgNight,(game.bg_x,0))
    blit(images.bgNight,(game.bg_x+576,0))
    blit(images.floor, (game.floor_x,900))
    blit(images.floor, (game.floor_x+576,900))
    blit(text(f'Score {int(game.highScore)}', True, white), (200,40))
    blit(text("Flappy BIRD UP!", True, orange), (130,100))
    blit(text("New game? :)", True, white), (150,200))
    blit(text("<Space, Click>", True, white), (150,250))
    birdUp = flip(images.birdUp, True, False) if game.birdupAnimation.switch else images.birdUp
    blit(birdUp, (100,350))
  else:
    pipe = game.images.pipeRed if game.doubleRound else game.images.pipe
    rect = pipesRect(game)
    blit(images.bg, (game.bg_x,0))
    blit(images.bg, (game.bg_x+576,0))
    bird = images.birdAnimation[game.bird_index]
    rotatedBird = pygame.transform.rotozoom(bird,-game.gravity * 3, 1)
    blit(rotatedBird, (50,game.bird_y))
    blit(pipe, rect.bottom)
    blit(flip(pipe, False, True), rect.top)
    blit(images.floor, (game.floor_x,900))
    blit(images.floor, (game.floor_x+576,900))
    blit(text(str(game.score), True, white), (270,80))
    if (game.freeze > 0):
      deathBG = pygame.Surface((576, 1024), pygame.SRCALPHA)
      deathBG.fill((255,0,0,200))
      blit(deathBG, (0,0))

  if replay:
    blit(text("Replay", True, (255,0,0)), (220,350))

  if game.smallResolution:
    game.screen.blit(scale(internalResolution, game.screen.get_size()), (0,0))
  else:
    game.screen.blit(internalResolution, (0,0))
  
  return game

def run(withReplay: bool = False, mute: bool = False, smallResolution: bool = False):
  game = init(mute, smallResolution)
  replay = []
  while game.running:
    game = draw(update(game, eventsToInput(pygame.event.get())))
    pygame.display.update()
    game.gameClock.tick(120)
    if (withReplay and not game.gameOver):
      replay.append(game)

  for r in replay:
    if (eventsToInput(pygame.event.get()).gameQuit):
      exit()
    draw(r, True)
    pygame.display.update()
    game.gameClock.tick(120)

  pygame.quit()
  exit()

if __name__ == "__main__":
  parser = ArgumentParser("Flappy Bird Up")
  parser.add_argument("-s", "--small", help="with small resolution", action="store_true")
  parser.add_argument("-r", "--replay", help="with replay function", action="store_true")
  parser.add_argument("-m", "--mute", help="mute audio", action="store_true")
  args = parser.parse_args()
  run(withReplay=args.replay, mute=args.mute, smallResolution=args.small)
