# FlappyBirdFP

## Getting started

Comparison imperative programming vs. functional programming using a small game.

## Dependencies

pygame https://pypi.org/project/pygame/

```
pip install pygame
```

## Test

Imperative example (run in folder):

```
python Original.py
```

Functional example (run in folder):

```
python FP.py
```

Options:

  -h, --help    show this help message and exit

  -s, --small   with small resolution

  -r, --replay  with replay function

  -m, --mute    mute audio